
            <!-- Footer -->
            <footer class="page-footer font-small pt-4" style="background-color: <?php echo $tmain; ?>;">
                <div class="footer-copyright text-center py-1 text-decoration-none text-reset">
                    <p style="color: #ffffff;">&copy; Copyright <a href="<?php echo $wlink; ?>" style="color: #ffffff;"><?php echo $wname; ?></a> <?php echo date("Y"); ?></p>
                </div>
            </footer>