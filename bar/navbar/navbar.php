    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" style="background-color: <?php echo $tmain; ?>;">
        <div class="container-fluid">
            <a class="navbar-brand" href="#"><img src="/<?php echo $froot; ?>/style/images/logo.png" height="20" style="margin-bottom: 2px;" alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarToggler">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item">
                        <a class="nav-item nav-link" href="<?php echo $plocationl_1; ?>" style="color: <?php echo $tsecondary; ?>;"><?php echo $pnamel_1; ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item nav-link" href="<?php echo $plocationl_2; ?>" style="color: <?php echo $tsecondary; ?>;"><?php echo $pnamel_2; ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item nav-link" href="<?php echo $plocationl_3; ?>" style="color: <?php echo $tsecondary; ?>;"><?php echo $pnamel_3; ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-item nav-link" href="<?php echo $plocationl_4; ?>" style="color: <?php echo $tsecondary; ?>;"><?php echo $pnamel_4; ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>