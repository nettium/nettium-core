<?php
######################
#                    #
#    Nettium-Core    #
#                    #
#       Config       #
#                    #
######################

// General configuration
    
    // Website name
    $wname = 'Nettium-Core';
    
    // Website link
    $wlink = 'https://nettium.eu/';
    
    // Website language (ISO Language codes)
    $wlang = 'en';
    
    // Version number of NCORE, please don't change this
    $version = '1.0.0';
    

// Menu settings
    
    // Menu buttons left
    $pnamel_1 = 'Page 1';
    $pnamel_2 = 'Page 2';
    $pnamel_3 = 'Page 3';
    $pnamel_4 = 'Page 4';
    
    // Page locations left
    $plocationl_1 = 'page1';
    $plocationl_2 = 'page2';
    $plocationl_3 = 'page3';
    $plocationl_4 = 'page4';

    // NOTE: Right side buttons are only used as a placeholder for future versions and won't show up on your site!
    // Menu buttons right
    $pnamer_1 = 'Login';
    $pnamer_2 = 'Register';
    
    // Page locations right
    $plocationr_1 = 'login';
    $plocationr_2 = 'register';
    
    // Menu name
    $mname = 'Menu';

// Folder settings
    
    // NCore folder (If NCore is in the root folder leave empty)
    $froot = '';
    
    // Homepage
    $fhome = 'page1.php';
    
// Theme settings
    
    // Main colour
    $tmain = '#333333';
    
    // Secondary colour
    $tsecondary = '#ffffff';
?>