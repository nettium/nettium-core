<!DOCTYPE html>
<html lang="<?php echo $wlang; ?>">
<head>
    <!-- Copyright Nettium -->
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    
    <!-- Config include -->
    <?php include './config.php'; ?>
    
    <!-- CSS includes -->
    <link rel="stylesheet" type="text/css" href="./style/style.css">
    <link rel="stylesheet" type="text/css" href="./style/bootstrap.css">
    
    <!-- Favicon include -->
    <link rel="icon" type="image/ico" href="<?php echo $froot; ?>/style/images/favicon.ico">
    
    <!-- Jquery include -->
    <script src="https://code.jquery.com/jquery-3.1.0.js"></script>
    
    <!-- Title of page -->
    <title><?php echo $wname; ?> - <?php echo $pnamel_1; ?></title>
    
    <!-- Bootstrap includes -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    
</head>
<body>
    <div id="page">
        
        <!-- Navbar include -->
        <div id="navbar">
            <?php include "./bar/navbar/navbar.php"; ?>
        </div>
        
        <!-- Main page -->
        <div id="main">
            <!-- Main text -->
            <p class="maintext" style="margin-top: 50px;vertical-align: baseline;">
                PAGE CONTENT
            </p>
        </div>
        
        <!-- Footer include -->
        <div id="footer">
            <?php include './bar/footer/footer.php'; ?>
        </div>
        
    </div>
</body>
</html>